<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- Add DOCTYPE -->
  <xsl:output method="html" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- Rewrite all href="http[s]://bugs.archlinux.org/task/<id>" to "<id>.html" -->
  <xsl:template match="a/@href[(starts-with(normalize-space(.), 'http://bugs.archlinux.org/task/') or starts-with(normalize-space(.), 'https://bugs.archlinux.org/task/')) and not(contains(., 'getfile') or contains(., '?'))]">
    <xsl:attribute name="href">
      <xsl:variable name="href" select="translate(. , '&#xA;', '')"/>
      <xsl:choose>
        <!-- task/21194.html https://bugs.archlinux.org/task/20596#comment65592 -->
        <xsl:when test="contains($href, '#')">
          <xsl:variable name="task-id" select="substring-before(substring-after($href, 'bugs.archlinux.org/task/'), '#')"/>
          <xsl:value-of select="concat($task-id, '.html#', substring-after($href, '#'))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat(substring-after($href, 'bugs.archlinux.org/task/'), '.html')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <!-- Change href for "Attached to Project: $PROJECT" to #  -->
  <xsl:template match="a/@href[contains(., '/index.php?project=')]">
    <xsl:attribute name="href">
      <xsl:value-of select="concat('#', substring-after(., '?'))"/>
    </xsl:attribute>
  </xsl:template>

  <!-- Remove link relation with the different projcets -->
  <xsl:template match="//link[@type='text/html']"/>
  <!-- Remove RSS feed links -->
  <xsl:template match="//link[@type='application/rss+xml']"/>
  <xsl:template match="//link[@type='application/atom+xml']"/>
  <!-- Remove links to Home, Packages, Forums etc. from the navbar -->
  <xsl:template match="//*[@id='archnavbarmenu']"/>
  <!-- Remove show task -->
  <xsl:template match="//*[@id='showtask']"/>
  <xsl:template match="//*[@id='title']"/>
  <!-- Remove login bar -->
  <xsl:template match="//*[@id='menu']"/>
  <!-- Remove "<Switch project>, Overview, Tasklist, Roadmap" bar -->
  <xsl:template match="//*[@id='pm-menu']"/>
  <!-- Remove intromessage: "Please read this before reporting a bug:" -->
  <xsl:template match="//*[@id='intromessage']"/>
  <!-- Remove Tasklist and Next task -->
  <xsl:template match="//*[@id='navigation']"/>

<!-- Fjern remove button -->

</xsl:stylesheet>
